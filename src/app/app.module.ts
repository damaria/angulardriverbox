import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { MainComponent } from './pages/main/main.component';
import { HomeComponent } from './pages/main/home/home.component';
import { StocktakingComponent } from './pages/main/stocktaking/stocktaking.component';
import { DepositToChashboxComponent } from './pages/main/deposit-to-chashbox/deposit-to-chashbox.component';
import { DepositToBankComponent } from './pages/main/deposit-to-bank/deposit-to-bank.component';
import { ChangingComponent } from './pages/main/changing/changing.component';
import { OweReportComponent } from './pages/main/owe-report/owe-report.component';
import { MoneyHeaderComponent } from './pages/main/components/money-header/money-header.component';
import { MoneyHeaderComponentComponent } from './pages/main/components/money-header/money-header-component/money-header-component.component';
import { KeysPipe } from './shared/pipes/keys.pipe';
import { MenuHeaderComponent } from './pages/main/components/menu-header/menu-header.component';
import { ToPromisePipe } from './shared/pipes/toPromise.pipe';
import { AmountInputComponent } from './shared/components/amount-input/amount-input.component';
import { LoginFormComponent } from './shared/components/login-form/login-form.component';
import { ModalComponent } from './shared/components/modal/modal.component';
import { SelectCashboxModalComponent } from './pages/login/components/select-cashbox-modal/select-cashbox-modal.component';
import { LoginPopUpComponent } from './pages/login/components/login-pop-up/login-pop-up.component';
import { PopUpComponent } from './shared/components/pop-up/pop-up.component';
import { InfoModalComponent } from './shared/components/modal/info-modal/info-modal.component';
import { GeneralComponent } from './pages/main/owe-report/general/general.component';
import { DetailsComponent } from './pages/main/owe-report/details/details.component';
import { SpinnerComponent } from './shared/components/spinner/spinner.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { UnloadDriverCardEnterComponent } from './pages/main/unload-driver-card/unload-driver-card-enter/unload-driver-card-enter.component';
import { UnloadDriverCardRouterComponent } from './pages//main/unload-driver-card/unload-driver-card-router/unload-driver-card-router.components';
import { UnloadSpecificDriverCardComponent } from './pages/main/unload-driver-card/unload-specific-driver-card/unload-specific-driver-card.component';
import { DepositDriverCardComponent } from './pages/main/unload-driver-card/deposit-driver-card/deposit-driver-card.component';
import { MainButtonComponent } from './shared/components/buttons/main-button/main-button.component';
import { SecondaryButtonComponent } from './shared/components/buttons/secondary-button/secondary-button.component';
import { DepositHistoryComponent } from './pages/main/deposit-history/deposit-history.component';
import { ExcelExportComponent } from './shared/components/excel-export/excel-export.component';
import { WithoutPapersPipe } from './shared/pipes/withoutPapers.pipe';
import { NotFoundComponent } from './pages/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    HomeComponent,
    MenuHeaderComponent,
    StocktakingComponent,
    DepositToChashboxComponent,
    DepositToBankComponent,
    ChangingComponent,
    OweReportComponent,
    MoneyHeaderComponent,
    MoneyHeaderComponentComponent,
    KeysPipe,
    ToPromisePipe,
    WithoutPapersPipe,
    AmountInputComponent,
    LoginFormComponent,
    ModalComponent,
    SelectCashboxModalComponent,
    LoginPopUpComponent,
    PopUpComponent,
    InfoModalComponent,
    GeneralComponent,
    DetailsComponent,
    SpinnerComponent,
    UnloadDriverCardEnterComponent,
    UnloadDriverCardRouterComponent,
    UnloadSpecificDriverCardComponent,
    DepositDriverCardComponent,
    MainButtonComponent,
    SecondaryButtonComponent,
    DepositHistoryComponent,
    ExcelExportComponent,
    NotFoundComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    Ng2SearchPipeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
