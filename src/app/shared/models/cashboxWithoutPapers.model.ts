import { cashboxDataType as type, getValue} from '../../shared/constants/server';
import { CompareResult } from '../constants/driverBox';
import { Cashbox } from './cashbox.model';

export class CashboxWithoutPapers {
    constructor() {
        this[type._10] = 0;
        this[type._50] = 0;
        this[type._100] = 0;
        this[type._200] = 0;
        this[type._500] = 0;
        this[type._1000] = 0;
        this[type._2000] = 0;
        this[type._5000] = 0;
        this[type._10000] = 0;
        this[type._20000] = 0;
    }

    increase(cashbox: Cashbox | CashboxWithoutPapers) {
        Object.keys(this).forEach(key => {
            if (cashbox[key]) {
                this[key] = this[key] + cashbox[key];
            }
        });
    }

    decrease(cashbox: Cashbox | CashboxWithoutPapers) {
        Object.keys(this).forEach(key => {
            if(cashbox[key]) {
                this[key] = this[key] - cashbox[key];
            }
        });
    }

    compare(cashbox: Cashbox | CashboxWithoutPapers) : CompareResult {
        if (this.getSum() != cashbox.getSum()) {
            return CompareResult.DIFFERENT_SUM;
        }

        let result = CompareResult.IDENTICAL;

        Object.keys(this).forEach(key => {
            if (cashbox[key] != null &&
                    this[key] != null &&
                        cashbox[key] != this[key]) {
                result = CompareResult.DIFFERENT_COIN_COMPOSITION;
            }
        })
        
        return result;
    }

    getSum() {
        let sum = 0;
        Object.keys(this).forEach(key => {
            sum += this[key] * getValue(key);
        });

        return sum/100;
    }

    getCoinSum(coin) {
        return (this[coin] * getValue(coin))/100;
    }

    convertToServerFormat() {
        let result = [];
        Object.keys(this).forEach(key => {
            if (this[key] != 0) {
                result.push({cashboxDataTypeID: key, cashboxCountCount: this[key].toString()});
            }
        })
        if (result.length == 0) {
            result.push({cashboxDataTypeID: "1", cashboxCountCount: "0"});
        }
        return result;
    }

    getCopy(): Cashbox {
        return Object.assign(Object.create(Object.getPrototypeOf(this)), this);
    }
}