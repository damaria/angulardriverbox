//For now, in the future will be switch statment
// export const baseUrl = 'https://ticketing.isrcorp.co.il:8093/isrTicketingTest/v1';
// export const baseUrlProd = 'https://isrticketing.isrcorp.co.il/isrTicketing/v1'; /*Prodaction on texi sever*/

export const baseUrl = 'https://cirtus.egged-taavura.co.il/isrTicketingTest/v1';

export const urls = {
    LOGIN: `${baseUrl}/driverCashbox/signIn`,
    LOGOUT: `${baseUrl}/driverCashbox/logout`,
    GET_DEVICE: `${baseUrl}/devices/getDevice`,
    GET_OPEN_LOGIN_SESSION: `${baseUrl}/driverCashbox/getOpenLoginSession`,
    GET_SHIFT_ID: `${baseUrl}/driverCashbox/getShiftId`,
    END_SHIFT: `${baseUrl}/driverCashbox/endShift`,
    START_SHIFT: `${baseUrl}/driverCashbox/startShift`,
    CHECK_LOGIN: `${baseUrl}/login/checkLogin`,
    GET_REP_Z: `${baseUrl}/driverCashbox/getRepZ`,
    ADD_REP_Z: `${baseUrl}/driverCashbox/addRepZ`,
    CLOSE_CASHBOX: `${baseUrl}/driverCashbox/closeCashbox`,
    GET_DRIVER_CASHBOX: `${baseUrl}/driverCashbox/getDriverCashboxTransaction`,
    ADD_DRIVER_CASHBOX: `${baseUrl}/driverCashbox/addDriverCashboxTransaction`,
    STOCKTAKING_WITH_LOCK: `${baseUrl}/driverCashbox/stocktakingWithLock`,
    GET_OWE_DRIVERS_REPORT: `${baseUrl}/driverCashbox/getOweDriversReport`,
    GET_DEBT_REPAYMENT_DETAILS: `${baseUrl}/driverCashbox/getDebtRepaymentDetails`,
    GET_SHIFT_TO_DEPOSIT: `${baseUrl}/driverCashbox/getShiftToDeposit`,
    GET_SHIFT_TO_DEPOSIT_HISTORY: `${baseUrl}/driverCashbox/getShiftToDepositHistory`,
    GET_EMPLOYEE_DETAILS: `${baseUrl}/employee/getEmployeeDetails`
}
