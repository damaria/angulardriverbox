export enum loginSource {
    BROWSER = '4'
}

export enum deviceType {
    CASHBOX = '2'
}

export enum shiftType {
    CASHBOX = '2'
}
export enum StatusCode {
    OPEN_SHIFT = '1',
    NEW_SHIFT = '2'
}

export enum androidAppVersion {
    '1.4.4' = '1.4.4'
}

export enum general {
    NULL = '-999'
}

export enum endShiftStatus {
    MANUAL = '2',
    AUTO = '4'
}

export enum typeRoleId {
    USER = '1',
    ADMIN = '3',
}

export enum cashboxStatus {
    OPEN = '1',
    LOCKED = '2'
}

export enum alsoOpenShifts {
    TRUE = '1',
    FALSE = '2'
}

export enum deposited {
    WITHOUT = '0',
    JUST = '1',
    ALSO = '',
}

export enum detailsSumCode {
    DETAILS = '1',
    SUM = '2',
    ALL = ''
}

export enum cashboxDataType {
    _10 = 1,
    _50 = 2,
    _100 = 3,
    _200 = 4,
    _500 = 5,
    _1000 = 6,
    _2000 = 7,
    _5000 = 8,
    _10000 = 9,
    _20000 = 10,
    cancel = 11,
    idfVoucher = 12,
    credit = 13
}

export enum actionTypeTransaction {
    ADD_TRANSACTION = "1",
    REMOVE_TRANSACTION = "2",
    UPDATE_TRANSACTION = "3",
    TAKE_OUT_MONEY_PROACTIVELY = "4",
    ENTER_MONEY_PROACTIVELY = "5",
    DEPOSIT_TRANSACTION = "6",
    FIX_CASHBOX_AMOUNT_PROACTIVELY = "7",
    REJECTION_FIX_CASHBOX = "8",
    DEBT_REPAYMENT = "9"
}

export function getValue(code) {
    if (code > 10) {
        return 0;
    }
    return +cashboxDataType[code].slice(1); 
}


interface dataClass {
    firebase: {},
    sessionData: {},
    userData: {}
}

export interface ServerResponse {
    data: dataClass | any,
    dateTime: string,
    description: string,
    descriptionForUserHE: string,
    newToken: boolean,
    statusCode: number
}
