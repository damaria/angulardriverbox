export enum sessionKeys {
    operatorId = 'operatorId',
    userId = 'userId',
    username = 'username',
    sessionData = 'sessionData',
    fullVersion = 'fullVersion',
    shift = 'shift',
    cashboxId = 'cashboxId',
    cashboxName = 'cashboxName',
    unloadCardDriverName = 'unloadCardDriverName',
    unloadCardShiftsToDeposit = 'unloadCardShiftsToDeposit'
}