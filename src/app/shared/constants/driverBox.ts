export const fullVersionOperators = [
    "4",
    "93",
    "34"
];

export enum CompareResult {
    IDENTICAL,
    DIFFERENT_COIN_COMPOSITION,
    DIFFERENT_SUM,
}