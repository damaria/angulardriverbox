import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'keys'})
export class KeysPipe implements PipeTransform {
  transform(value): any {
    if (typeof(value) == 'undefined') {
      return;
    }
    let keys = [];

    for (let key in value) {
      if (+key < 11) {
        keys.push({key: key, value: value[key]});
      }
    }
    keys.reverse();

    for (let key in value) {
      if (+key >= 11) {
        keys.push({key: key, value: value[key]});
      }
    }
    
    return keys;
  }
}