import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'toPromise'})
export class ToPromisePipe implements PipeTransform {
  transform(value): any {
    return new Promise((resolve, reject) => {
      resolve(value);
    });
  }
}