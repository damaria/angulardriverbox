import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'withoutPapers'})
export class WithoutPapersPipe implements PipeTransform {
  transform(value): any {
    const res =  value.filter(item => +item.key <= 10);
    return res;
  }
}