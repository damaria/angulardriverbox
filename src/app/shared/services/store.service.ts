import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { urls } from "../../shared/constants/URLs";
import * as serverConstant from "../../shared/constants/server";
import { Cashbox } from "../models/cashbox.model";
import { Subject } from "rxjs";
import { tap } from 'rxjs/operators';
import { sessionKeys } from "../constants/sessionKey";

@Injectable({providedIn: 'root'})
export class StoreService {
    private cashBox: Cashbox;
    private cashBoxSum;
    cashBoxChanged = new Subject();
    
    constructor (private http: HttpClient) {
            this.cashBox = new Cashbox();
        }

    updateCashBox() {
        return this.http
        .post(
            urls.GET_DRIVER_CASHBOX,
            {
                data: {
                    driverCashboxTransactionId: serverConstant.general.NULL,
                    idNumber: serverConstant.general.NULL,
                    startDT: serverConstant.general.NULL,
                    endDT: serverConstant.general.NULL,
                    device_ID: sessionStorage.getItem(sessionKeys.cashboxId),
                    onlySum: true
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            }
        )
        .pipe(
            tap((res: any) => {
                let sum = 0;
                res.data.forEach(item => {
                    let type = item.CashboxDataType_ID;
                    if (type == -15) {
                        type = 12;
                    }
                    const count = +item.CashboxDataCount;
                    this.cashBox[type] = count;
                    sum += +item.sumCash;
                });
                this.cashBoxSum = sum/100;
                    
                this.cashBoxChanged.next({
                    cashBox: this.cashBox,
                    cashBoxSum: this.cashBoxSum
                });
            })
        )
    }

    getCashbox(): Cashbox {
        return Object.assign(Object.create(Object.getPrototypeOf(this.cashBox)), this.cashBox);
    }

    getCashBoxSum() {
        return this.cashBoxSum;
    }
}