import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginFormService } from './login-form.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  @ViewChild('loginForm') loginForm: NgForm;
  @Input() type;

  constructor(private loginFormService: LoginFormService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.loginFormService.loginSubmited.next({
      formType: this.type,
      formData: this.loginForm.value
    });
  }

}
