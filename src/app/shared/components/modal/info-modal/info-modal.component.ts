import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-info-modal',
  templateUrl: './info-modal.component.html',
  styleUrls: ['./info-modal.component.css']
})
export class InfoModalComponent implements OnInit {
  @Input('id') id;
  @Input('backgroundClickFunction') backgroundClickFunction;
  @Input('icon') icon;
  @Input('title') title;
  @Input('subTitle') subTitle;
  @Input('hr') hr;
  @Input('btnTitle') btnTitle;
  @Input('btnRightText') btnRightText;
  @Input('btnRightClickFun') btnRightClickFun;
  @Input('btnLeftText') btnLeftText;
  @Input('btnLeftClickFun') btnLeftClickFun;
  @Input('error') error;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

}
