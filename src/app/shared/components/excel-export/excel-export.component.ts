import { AfterViewInit, Component, Input } from '@angular/core';
import * as XLSX from 'xlsx'; 

@Component({
  selector: 'app-excel-export',
  templateUrl: './excel-export.component.html',
  styles: [
    'img { height: 20px; cursor: pointer; }'
  ]
})
export class ExcelExportComponent implements AfterViewInit{
  @Input('table') table;
  @Input('fileName') fileName;
  @Input('color') color;
  src;
  fileExtension = '.xlsx';

  ngAfterViewInit() {
    switch(this.color) {
      case 'black':
        this.src = "assets/icons/downloadBlackIcon.svg"
        break;
      case 'white':
        this.src = "assets/icons/downloadWhiteIcon.svg"
        break;
    }
  }

  excelExport = () => {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName + this.fileExtension);		
  }
}
