import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

export interface subData {
    operation: string,
    type: string,
    amount?: number,
    payload?: string,
    info?: string
    maxValue?: number
} 

@Injectable({providedIn: 'root'})
export class AmountInputService {
    inputChanged = new Subject<subData>();
}