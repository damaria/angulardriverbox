export function isToday(date) {
    const pureDate = new Date(date).setHours(0,0,0,0);
    const today = new Date().setHours(0,0,0,0);
    return pureDate == today;
}

export function getCleanDate(fullDate) {
    const date = new Date(fullDate + "UTC");
    const result = twoDigits(date.getDate()) + '/' + twoDigits(date.getMonth() + 1) + '/' + date.getFullYear();
    return result;
}

export function getCleanTime(fullDate) {
    const date = new Date(fullDate + "UTC");
    const result = twoDigits(date.getHours()) + ':' + twoDigits(date.getMinutes());
    return result;
}

function twoDigits(num) {
    let result = num;
    if (num < 10) {
        result = "0" + num;
    }
    return result;
}