// import { getValue } from "../constants/server";

export function isToday(date) {
    const pureDate = new Date(date).setHours(0,0,0,0);
    const today = new Date().setHours(0,0,0,0);
    return pureDate == today;
}

export function getImageUrl(type) {
    return "assets/cashboxDataType/" + type + ".png";
}

// export function getSum(depositCashbox) {
//     let sum = 0;
//     Object.keys(depositCashbox).forEach(key => {
//         sum += depositCashbox[key] * getValue(key)
//     });

//     return sum/100;
// }

// export function getCoinSum(coin, depositCashbox) {
//     return (depositCashbox[coin] * getValue(coin))/100;
// }



// export function convertToServerFormat(cashbox) {
//     let result = [];
//     Object.keys(cashbox).forEach(key => {
//       result.push({cashboxDataTypeID: key, cashboxCountCount: cashbox[key].toString()});
//     })
//     return result;
// }