import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { map, switchMap } from 'rxjs/operators';
import { Observable } from "rxjs";

import { fullVersionOperators } from '../../shared/constants/driverBox';
import { androidAppVersion, ServerResponse } from "../../shared/constants/server";
import * as serverConstant from '../../shared/constants/server';
import { DescriptionNoOk, LoginResponse, LoginResponseType } from './loginResponse';
import { isToday } from "src/app/shared/utils/date";
import { SelectCashboxModalService } from "./components/select-cashbox-modal/select-cashbox-modal.service";
import { urls } from "../../shared/constants/urls";
import { sessionKeys } from "src/app/shared/constants/sessionKey";

@Injectable({providedIn: 'root'})
export class LoginService {
    // operatorId;
    // sessionData;
    // fullVersion;
    // cashboxId;
    // cashboxName;
    // shift;
    // answer;

    constructor(
        private http: HttpClient,
        private route: ActivatedRoute,
        private router: Router,
        private selectCashboxModalService: SelectCashboxModalService
        ) { }

    // resetValues() {
    //     this.operatorId = null;
    //     this.sessionData = null;
    //     this.fullVersion = false;
    //     this.cashboxId = null;
    //     this.cashboxName = null;
    //     this.shift = null;
    // }

    login(email, password) {
        if (this.getOperatorId() == null) {
            return new Observable((observer) => {
                observer.next(new LoginResponse(
                    LoginResponseType.noOperatorId
                ));
            })
        }

        return this.http
            .post<ServerResponse>(
                urls.LOGIN,
                {
                    email: email,
                    password: password,
                    operator_id: sessionStorage.getItem(sessionKeys.operatorId),
                    loginSource: serverConstant.loginSource.BROWSER,
                    withUserData: "1"
                })
                .pipe(
                    switchMap(res => {
                        if (res.description == 'ok') {
                            const data = res.data;
                            // this.sessionData = res.data.sessionData;
                            sessionStorage.setItem(sessionKeys.sessionData , JSON.stringify(data.sessionData));
                            
                            sessionStorage.setItem(sessionKeys.username , data.userData.firstName);
                            sessionStorage.setItem(sessionKeys.userId , data.userData.idNumber);
                            
                            const fullVersion = fullVersionOperators.includes(data.sessionData.operator_id);
                            sessionStorage.setItem(sessionKeys.fullVersion , fullVersion.toString());
                            
                            //in the future - if have 'users' -
                            //probably user return from break -
                            //doesnt need to get device, go to selectBox()

                            return this.getDevice();
                        }
                        else {
                            return new Observable((observer) => {
                                observer.next(new LoginResponse(
                                    LoginResponseType.descriptionNoOk,
                                    DescriptionNoOk.login 
                                ));
                            })
                        }
                    })
                )
    }

    adminLogin(email, password) {
        return this.http
            .post<ServerResponse>(
                urls.LOGIN,
                {
                    email: email,
                    password: password,
                    operator_id: sessionStorage.getItem(sessionKeys.operatorId),
                    loginSource: serverConstant.loginSource.BROWSER,
                    withUserData: "1"
                })
                .pipe(
                    switchMap(res => {
                        if (res.description != 'ok') {
                            return new Observable((observer) => {
                                observer.next(new LoginResponse(
                                    LoginResponseType.descriptionNoOk,
                                    DescriptionNoOk.loginAdmin
                                ));
                            })
                        }
                        else {
                            const roles = res.data.userData.typeRoleId;
                            if (!roles.includes(serverConstant.typeRoleId.ADMIN)) {
                                return new Observable((observer) => {
                                    observer.next(new LoginResponse(
                                        LoginResponseType.userNoAdmin
                                    ));
                                })
                            }
                            else {
                                return this.logoutFromSession(res.data.sessionData)
                                .pipe(
                                    switchMap((res: LoginResponse) => {
                                        switch(res.type) {
                                            case LoginResponseType.descriptionNoOk:
                                                return new Observable((observer) => {
                                                    observer.next(new LoginResponse(
                                                        LoginResponseType.descriptionNoOk,
                                                        DescriptionNoOk.logoutFromSession
                                                    ));
                                                })
                                            case LoginResponseType.logoutFromSessionSuccessfully:
                                                return new Observable((observer) => {
                                                    observer.next(new LoginResponse(
                                                        LoginResponseType.adminLoginSuccesfully
                                                    ));
                                                })
                                        }
                                    })
                                );
                            }
                        }
                    })
                )
    }

    disconnect() {
        return this.endShift(
            JSON.parse(sessionStorage.getItem(sessionKeys.shift)),
            serverConstant.endShiftStatus.MANUAL
            )
        .pipe(
            switchMap((res: LoginResponse) => {
                switch(res.type) {
                    case LoginResponseType.descriptionNoOk:
                        return new Observable((observer) => {
                            observer.next(new LoginResponse(
                                LoginResponseType.descriptionNoOk,
                                DescriptionNoOk.endShift 
                            ));
                        })
                    case LoginResponseType.endShiftSuccessfully:
                        return this.logoutFromSession(JSON.parse(sessionStorage.getItem(sessionKeys.sessionData)));
                }
            })
        )
    }

    getOperatorId() {
        const queryParams = this.route.snapshot.queryParams;
        const operatorId = Object.keys(queryParams)[0];
        sessionStorage.setItem(sessionKeys.operatorId, operatorId);
        return operatorId;
    }

    getDevice() {
        return this.http
            .post<ServerResponse>(
                urls.GET_DEVICE,
                {
                    data: {
                        active: "1",
                        status_id: serverConstant.general.NULL,
                        deviceType: serverConstant.deviceType.CASHBOX
                    },
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                })
                .pipe(
                    switchMap(res => {
                        const cashboxes = res.data;
                        if (res.description !== 'ok' || cashboxes.length == 0) {
                            return new Observable((observer) => {
                                observer.next(new LoginResponse(
                                    LoginResponseType.descriptionNoOk,
                                    DescriptionNoOk.getDevice
                                ));
                            })
                        }
                        else {
                            if (cashboxes.length == 1) {
                                return this.checkOpenSessions(res.data[0].Device_id, res.data[0].DeviceDescr);
                            }
                            else { 
                                return new Observable((observer) => {
                                    observer.next(new LoginResponse(
                                        LoginResponseType.selectCashbox,
                                        cashboxes
                                    ));
                                })
                            }
                        }
                    })
                )
    }

    checkOpenSessions(cashboxId, cashboxName) {
        // this.cashboxId = cashboxId;
        sessionStorage.setItem(sessionKeys.cashboxId,cashboxId);
        // this.cashboxName = cashboxName;
        sessionStorage.setItem(sessionKeys.cashboxName,cashboxName);

        return this.http
            .post<ServerResponse>(
                urls.GET_OPEN_LOGIN_SESSION,
                {
                    data: {
                        idNumber: serverConstant.general.NULL,
                        deviceId: cashboxId,
                        deviceType: serverConstant.deviceType.CASHBOX,
                        employeeType: serverConstant.general.NULL,
                        loginSource: serverConstant.loginSource.BROWSER,
                        shiftType: serverConstant.general.NULL,
                        closeShift: "0"
                    },
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                })
                .pipe(
                    switchMap(res => {
                        if (res.description !== 'ok') {
                            this.selectCashboxModalService.cancelCashboxSelected.next();
                            return new Observable((observer) => {
                                observer.next(new LoginResponse(
                                    LoginResponseType.descriptionNoOk,
                                    DescriptionNoOk.getOpenLoginSession
                                ));
                            })
                        }
                        else {
                            const sessionData = JSON.parse(sessionStorage.getItem(sessionKeys.sessionData));
                            const openSessions = res.data.filter(item => item.session !== sessionData.session)
                            //no else session connected to this cashbox
                            if (openSessions.length == 0) {
                                return this.getIntoCheckBox();
                            }
                            //else session connected to this cashbox
                            else {
                                //same user connected
                                this.selectCashboxModalService.cancelCashboxSelected.next();
                                if (openSessions.every(session =>
                                    session.FB_uid == sessionData.uid
                                ))
                                {
                                    return new Observable((observer) => {
                                        observer.next(new LoginResponse(
                                            LoginResponseType.openInDifferentSession,
                                            openSessions    
                                        ));
                                    })
                                }
                                //other user connected
                                else {
                                    return new Observable((observer) => {
                                        observer.next(new LoginResponse(
                                            LoginResponseType.openByOtherUser,
                                            openSessions
                                        ));
                                    })
                                }
                            }
                        }
                    })
                )
    }

    logoutFromSession(session) {
        return this.http
        .post<ServerResponse>(
            urls.LOGOUT,
            session.FB_uid ?
            {
                uid: session.FB_uid,
                session: session.session,
                operator_id: session.Operator_ID
            } : 
            {
                uid: session.uid,
                session: session.session,
                operator_id: session.operator_id
            })
            .pipe(
                switchMap(res => {
                    if (res.description !== 'ok') {
                        return new Observable((observer) => {
                            observer.next(new LoginResponse(
                                LoginResponseType.descriptionNoOk,
                                DescriptionNoOk.logoutFromSession 
                            ));
                        })
                    }
                    else {
                        return new Observable((observer) => {
                            observer.next(new LoginResponse(
                                LoginResponseType.logoutFromSessionSuccessfully,
                            ));
                        })
                    }
                })
            )
    }

    getIntoCheckBox() {
        if (sessionStorage.getItem(sessionKeys.fullVersion)) {
            return this.getRepZ();  
        }

        else {
            return this.getShiftId();
        }
    }

    getShiftId() {
        return this.http
            .post<ServerResponse>(
                urls.GET_SHIFT_ID,
                {
                    data: {
                        idNumber: sessionStorage.getItem(sessionKeys.userId),
                        device_ID: sessionStorage.getItem(sessionKeys.cashboxId),
                        shiftType:  serverConstant.shiftType.CASHBOX
                    },
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                })
                .pipe(
                    switchMap(res => {
                        if (res.description !== 'ok') {
                            return new Observable((observer) => {
                                observer.next(new LoginResponse(
                                    LoginResponseType.descriptionNoOk,
                                    DescriptionNoOk.getShiftId 
                                ));
                            })
                        }
                        else {
                            const shift = res.data[0]
                            sessionStorage.setItem(sessionKeys.shift, JSON.stringify(shift));
                            if (shift.StatusCode == serverConstant.StatusCode.NEW_SHIFT) {
                                return this.startShift();
                            }
                            else { //open shift
                                if (isToday(shift.Shift_Start)) {
                                    return new Observable((observer) => {
                                        observer.next(new LoginResponse(
                                            LoginResponseType.canEnterToCashbox
                                        ));
                                    });
                                }
                                else {
                                    return this.endShift(shift, serverConstant.endShiftStatus.AUTO);
                                }
                            }
                        }
                    })
                )
    }

    getRepZ() {
        return this.http
        .post<ServerResponse>(
            urls.GET_REP_Z,
            {
                data: {
                    deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
                    startDt:  serverConstant.general.NULL,
                    endDt: serverConstant.general.NULL,
                    lastOpenCashbox: "1"
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            })
            .pipe(
                switchMap(res => {
                    if (res.description !== 'ok') {
                        return new Observable((observer) => {
                            observer.next(new LoginResponse(
                                LoginResponseType.descriptionNoOk,
                                DescriptionNoOk.getRepZ 
                            ));
                        })
                    }
                    else {
                        const repZ = res.data[0];
                        if (repZ == null) {
                            return this.addRepZ();
                        }
                        if (!repZ.closeCashboxDt) {
                            if (isToday(repZ.openCashboxDt)) { //middle of the day
                                return this.getShiftId();
                            }
                            else  { //repZ from the past no closed, need to close and search repZ again
                                return this.closeRepZ(repZ);
                            }
                        }
                        else {
                            if (!isToday(repZ.closeCashboxDt)) { //no repZ for today, need to open a new one
                                return this.addRepZ();
                            }
                            if (isToday(repZ.openCashboxDt)) { //after end of the day
                                return new Observable((observer) => {
                                    observer.next(new LoginResponse(
                                        LoginResponseType.alreadyClosedDay,
                                    ));
                                })
                            }
                            else  { //repZ open in the past and closed today
                                return this.addRepZ();
                            }
                        }  
                    }
                })
            );
    }

    addRepZ() {
        return this.http.
        post<ServerResponse>(
            urls.ADD_REP_Z,
            // JSON.stringify(
            {
                data: {
                    deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
                    openCashboxDt: new Date().getTime()
                    // openCashboxDt: new Date().getTime().toString()
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            }
            // )
        )
        .pipe(
            switchMap(res => {
                if (res.description !== 'ok') {
                    return new Observable((observer) => {
                        observer.next(new LoginResponse(
                            LoginResponseType.descriptionNoOk,
                            DescriptionNoOk.getRepZ 
                        ));
                    })
                }
                else {
                    return this.getShiftId();
                }
            })
        );
    }

    closeRepZ(repZ) {
        return this.http
        .post<ServerResponse>(
            urls.CLOSE_CASHBOX,
            {
                data: {
                    repZId: repZ.repZId,
                    closeCashboxDt: new Date().getTime()
                },
                sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            }
        )
        .pipe(
            switchMap(res => {
                if (res.description !== 'ok') {
                    return new Observable((observer) => {
                        observer.next(new LoginResponse(
                            LoginResponseType.descriptionNoOk,
                            DescriptionNoOk.closeRepZ 
                        ));
                    })
                }
                else {
                    return this.getRepZ();
                }
            })
        );
    }

    startShift() {
        const now = new Date();

        const shiftId = (JSON.parse(sessionStorage.getItem(sessionKeys.shift))).Shift_id;

        const shift = {
            androidAppVersion: androidAppVersion["1.4.4"],
            closeShiftForce: false,
            deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
            driverId: sessionStorage.getItem('userId'),
            Operator_id: sessionStorage.getItem(sessionKeys.operatorId),
            operator: sessionStorage.getItem(sessionKeys.operatorId),
            routeSystem: 0,
            serialNumberDevice: "aaaaaaaaaa",
            shiftId: shiftId,
            Shift_Start: new Date().toUTCString(),
            startShift: {
                date: {
                    day: now.getUTCDate(),
                    month: now.getUTCMonth() + 1,
                    year: now.getUTCFullYear()
                },
                time: {
                    hour: now.getUTCHours(),
                    minute: now.getMinutes(),
                    nano: 0,
                    second: now.getSeconds()
                }
            },
            status: 1,
            subContractor: 0,
            shiftType: serverConstant.shiftType.CASHBOX
        }

        sessionStorage.setItem(sessionKeys.shift, JSON.stringify(shift));

        return this.http
            .post<ServerResponse>(
                urls.START_SHIFT,
                    {
                        shift: JSON.stringify(shift),
                        sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                    }
                )
                .pipe(
                    switchMap(res => {
                        if (res.description !== 'ok') {
                            return new Observable((observer) => {
                                observer.next(new LoginResponse(
                                    LoginResponseType.descriptionNoOk,
                                    DescriptionNoOk.startShift 
                                ));
                            })
                        }
                        else {
                            return new Observable((observer) => {
                                observer.next(new LoginResponse(
                                    LoginResponseType.canEnterToCashbox
                                ));
                            });
                        }
                    })
                )
    }

    endShift(shift, status: serverConstant.endShiftStatus): Observable<any>{
        const shiftStart = new Date(shift.Shift_Start + ' UTC');
        const now = new Date();

        return this.http
        .post<ServerResponse>(
            urls.END_SHIFT,
                JSON.stringify({
                    shift: JSON.stringify({
                        androidAppVersion: androidAppVersion["1.4.4"],
                        closeShiftForce: false,
                        deviceId: sessionStorage.getItem(sessionKeys.cashboxId),
                        driverId: shift.Driver_id,
                        endShift: {
                            date: {
                                day: now.getUTCDate(),
                                month: now.getUTCMonth() + 1,
                                year: now.getUTCFullYear()
                            },
                            time: {
                                hour: now.getUTCHours(),
                                minute: now.getMinutes(),
                                nano: 0,
                                second: now.getSeconds()
                            }
                        },
                        operator: shift.Operator_id,
                        routeSystem: 0,
                        serialNumberDevice: 'aaaaaaaaaa',
                        shiftId: shift.Shift_id | shift.shiftId,
                        startShift: {
                            date: {
                                day: shiftStart.getUTCDate(),
                                month: shiftStart.getUTCMonth() + 1,
                                year: shiftStart.getUTCFullYear()
                            },
                            time: {
                                hour: shiftStart.getUTCHours(),
                                minute: shiftStart.getMinutes(),
                                nano: 0,
                                second: shiftStart.getSeconds()
                            }
                        },
                        status: 1,
                        subContractor: 0,
                        shiftType: serverConstant.shiftType.CASHBOX,
                        sumShiftMoney: 0,
                        closeShiftType: status
                    }),
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                })
            )
            .pipe(
                switchMap(res => {
                    if (res.description !== 'ok') {
                        return new Observable((observer) => {
                            observer.next(new LoginResponse(
                                LoginResponseType.descriptionNoOk,
                                DescriptionNoOk.endShift 
                            ));
                        })
                    }
                    else {
                        switch (status) {
                            case serverConstant.endShiftStatus.AUTO:
                                return this.getShiftId();
                            case serverConstant.endShiftStatus.MANUAL:
                                return new Observable((observer) => {
                                    observer.next(new LoginResponse(
                                        LoginResponseType.endShiftSuccessfully,
                                    ));
                                }) 
                        }
                    }
                })
            )
    }

    checkLogin() {
        const sessionData = JSON.parse(sessionStorage.getItem(sessionKeys.sessionData));

        return this.http
        .post<ServerResponse>(
            urls.CHECK_LOGIN, 
            {
                uid: sessionData.uid,
                session: sessionData.session,
                operator_id: sessionData.operator_id
            })
            .pipe(
                map(res => {
                    if (res.description !== 'ok') {
                        const queryParams = {}
                        queryParams[sessionStorage.getItem(sessionKeys.operatorId)] = "";
                        queryParams["operatorId"] = sessionStorage.getItem(sessionKeys.operatorId);
                        queryParams["sessionNoValid"] = true;
                        this.router.navigate(['login'], {queryParams: queryParams})
                        
                        return false;
                    }
                    else {
                        return true;
                    }
                })
            )
    }
}