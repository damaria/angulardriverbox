import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { StoreService } from 'src/app/shared/services/store.service';
import { ModalService } from '../../shared/components/modal/modal.service';
import { LoginPopUpService } from './components/login-pop-up/login-pop-up.service';
import { SelectCashboxModalService } from './components/select-cashbox-modal/select-cashbox-modal.service';
import { LoginService } from './login.service';
import { DescriptionNoOk, LoginResponse, LoginResponseType } from './loginResponse';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  @ViewChild('loginForm') loginForm: NgForm;
  @ViewChild('loginAdminForm') loginAdminForm: NgForm;
  
  isLoading;
  isLoadingAdmin;
  error;
  errorAdmin;
  devices;
  userOpenSessions;
  otherOpenSessions;
  adminLogin;
  adminLoginData;
  cashboxSelected = new Subscription();
  popUpClosed = new Subscription();

  sessionStorage = sessionStorage;
  sessionKeys = sessionKeys;

  constructor(
    private modalService: ModalService,
    private selectCashboxModalService: SelectCashboxModalService,
    public loginService: LoginService,
    private loginPopUpService: LoginPopUpService,
    private route: ActivatedRoute,
    private router: Router,
    private storeService: StoreService
    ) { }

  ngOnInit(): void {
    this.isLoading = false;
    this.resetValues();
    this.cashboxSelected = this.selectCashboxModalService.cashboxSelected.subscribe(
      cashbox => {
        this.loginPopUpService.closed.next();
        this.onSelectCashbox(cashbox);
      }
    );

    this.popUpClosed = this.loginPopUpService.closed.subscribe(
      () => {
        this.userOpenSessions = null;
        this.otherOpenSessions = null;
      }
    );

    if (this.route.snapshot.queryParamMap.get('sessionNoValid')) { 
      this.error = 'מישהו ניתק אותך, אנא התחבר מחדש.'
    }
    this.removeQueryParams();
  }

  login() {
    this.error = null;
    this.isLoading = true;
    
    this.loginService.login(this.loginForm.value.email, this.loginForm.value.password)
    .subscribe(
      (res: LoginResponse) => this.loginSubscribeHandler(res),
      error => this.loginSubscribeErrorHandler(error)
    );
  }

  loginAdmin() {
    this.errorAdmin = null;
    this.isLoadingAdmin = true;
      this.loginService.adminLogin(this.loginAdminForm.value.email, this.loginAdminForm.value.password)
      .subscribe(
        (res: LoginResponse) => this.loginSubscribeHandler(res),
        error => this.loginSubscribeErrorHandler(error)
      );
  }

  onSelectCashbox(cashbox) {
    this.loginService.checkOpenSessions(cashbox.Device_id, cashbox.DeviceDescr)
    .subscribe(
      (res: LoginResponse) => this.loginSubscribeHandler(res),
      error => this.loginSubscribeErrorHandler(error)
    );
  }

  getIntoCheckBox() {
    this.isLoading = true;
    this.resetValues();

    this.loginService.getIntoCheckBox()
    .subscribe(
      (res: LoginResponse) => this.loginSubscribeHandler(res),
      error => this.loginSubscribeErrorHandler(error)
    );
  }

  logoutFromSessions(openSessions) {
    this.isLoading = true;
    this.modalService.close('select-box-modal');
    this.resetValues();

    openSessions.forEach(session => {
      this.loginService.logoutFromSession(session)
      .subscribe(
        (res: LoginResponse) => {
          this.loginSubscribeHandler(res),
          error => this.loginSubscribeErrorHandler(error)
        } 
      );
    });
  }

  enterToCashbox() {
    // this.storeService.updateCashBox().subscribe(
    //   () => {
    //     this.router.navigate(['/main']);
    //   }
    // );
    this.router.navigate(['/main']);
  }

  loginSubscribeHandler (res: LoginResponse) {
    this.isLoading = false;
    this.isLoadingAdmin = false;
      
    switch(res.type) {
      case LoginResponseType.logoutFromSessionSuccessfully:
        this.getIntoCheckBox();
        break;
      case LoginResponseType.noOperatorId:
        this.error = "לא קיים קוד מפעיל ב-URL"
        break;
      case LoginResponseType.descriptionNoOk:
        switch(res.data) {
          case DescriptionNoOk.login:
            this.error = "שם משתמש או סיסמה אינם נכונים"
            break;
          case DescriptionNoOk.loginAdmin:
              this.errorAdmin = "שם משתמש או סיסמה אינם נכונים"
              break;
          case DescriptionNoOk.getDevice:
            this.error = "לא נמצאו מכשירים למשתמש זה"
            break;
          case DescriptionNoOk.getOpenLoginSession:
            this.error = "אין אפשרות לבדוק סשנים פתוחים"
            break;
          case DescriptionNoOk.getShiftId:
            this.error = "לא ניתן לקבל מספר משמרת"
            break;
          case DescriptionNoOk.endShift:
            this.error = "התרחשה בעיה בסגירת משמרת קודמת"
            break;
          case DescriptionNoOk.startShift:
            this.error = "התרחשה בעיה בפתיחת משמרת"
            break;
          case DescriptionNoOk.logoutFromSession:
            this.error = "התרחשה בעיה בהתנתקות מהסשנים הקודמים"
          case DescriptionNoOk.getRepZ:
            this.error = 'התרחשה בעיה בקבלת דו"ח Z'
          case DescriptionNoOk.getRepZOK:
            this.error = 'הכל טוסט בקבלת דו"ח Z'
          case DescriptionNoOk.closeRepZ:
            this.error = sessionStorage.getItem(sessionKeys.cashboxName) + "התרחשה בעיה בביצוע סגירת יום ל "
        }
        break;
      case LoginResponseType.selectCashbox:
        this.devices = res.data;
        this.modalService.open('select-box-modal');
        break;
      case LoginResponseType.openInDifferentSession:
        this.userOpenSessions = res.data;
        break;
      case LoginResponseType.openByOtherUser:
        this.otherOpenSessions = res.data;
        break;
      case LoginResponseType.canEnterToCashbox:
        this.enterToCashbox();
        break;
      case LoginResponseType.userNoAdmin:
        this.errorAdmin = "המשתמש אינו מנהל";
        break;
      case LoginResponseType.adminLoginSuccesfully:
        this.logoutFromSessions(this.otherOpenSessions);
        break;
      case LoginResponseType.alreadyClosedDay:
        this.error = 'עבור ' +  sessionStorage.getItem(sessionKeys.cashboxName)  + ' התבצעה סגירת יום. ניתן לפתוח משמרת רק ממחר.'
    }
  }

  loginSubscribeErrorHandler (error) {
    this.isLoading = false;
    console.error(error);
    this.error = error.message;
  }
 
  resetValues() {
    this.error = null;
    this.errorAdmin = null;
    this.devices = null;
    this.userOpenSessions = null;
    this.otherOpenSessions = null;
    this.adminLoginData = null;
    // this.cashboxSelected = false;
  }

  removeQueryParams() {
    const operatorId = this.route.snapshot.queryParamMap.keys[0];
    const queryParams = {};
    queryParams[operatorId] = "";
    this.router.navigate(
      [], 
      { queryParams: queryParams }
    );
  }

  ngOnDestroy() {
    this.cashboxSelected.unsubscribe();
    this.popUpClosed.unsubscribe();
  }
}