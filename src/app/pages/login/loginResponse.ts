export class LoginResponse {
    constructor(
        public type: LoginResponseType,
        public data?: any
        ) {}
}

export enum LoginResponseType {
    noOperatorId,
    selectCashbox,
    openInDifferentSession,
    openByOtherUser,
    closeDayDone,
    descriptionNoOk,
    canEnterToCashbox,
    logoutFromSessionSuccessfully,
    endShiftSuccessfully,
    checkLoginSeccesfully,
    userNoAdmin,
    adminLoginSuccesfully,
    alreadyClosedDay,
    stocktackingSuccess,
    stocktackingSuccessNoIdentical,
    cashboxLocked
}

export enum DescriptionNoOk {
    login,
    loginAdmin,
    getDevice,
    getOpenLoginSession,
    getShiftId,
    endShift,
    startShift,
    logoutFromSession,
    checkLogin,
    getRepZ,
    getRepZOK,
    closeRepZ
}