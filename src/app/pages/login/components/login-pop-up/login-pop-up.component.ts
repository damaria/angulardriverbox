import { Component, OnInit } from '@angular/core';
import { LoginPopUpService } from './login-pop-up.service';

@Component({
  selector: 'app-login-pop-up',
  templateUrl: './login-pop-up.component.html',
  styleUrls: ['./login-pop-up.component.css']
})
export class LoginPopUpComponent implements OnInit {
  constructor(private loginPopUpService: LoginPopUpService) { }

  ngOnInit(): void {
  }

  onClose() {
    this.loginPopUpService.closed.next();
  }
}
