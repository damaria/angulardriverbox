import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({providedIn: 'root'})
export class SelectCashboxModalService {
    cashboxSelected = new Subject;
    cancelCashboxSelected = new Subject;
}