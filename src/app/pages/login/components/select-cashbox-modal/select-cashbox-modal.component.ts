import { Component, Input, OnInit } from '@angular/core';
import { SelectCashboxModalService } from './select-cashbox-modal.service';
import { cashboxStatus } from '../../../../shared/constants/server';

@Component({
  selector: 'app-select-cashbox-modal',
  templateUrl: './select-cashbox-modal.component.html',
  styleUrls: ['./select-cashbox-modal.component.css']
})
export class SelectCashboxModalComponent implements OnInit {
  @Input('id') id: string;
  @Input('cashboxList') cashboxList;
  selected;
  cashboxStatus = cashboxStatus;

  constructor(private selectCashboxModalService: SelectCashboxModalService) {
    this.selected = null;
  }

  ngOnInit(): void {
    this.selectCashboxModalService.cancelCashboxSelected.subscribe(
      () => {
        this.selected = null;
      }
    ); 
  }

  onSelect(cashbox, id) {
    this.selected = id;
    this.selectCashboxModalService.cashboxSelected.next(cashbox);
  }

}
