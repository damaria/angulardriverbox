import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ServerResponse } from 'src/app/shared/constants/server';
import { urls } from 'src/app/shared/constants/urls';
import * as serverConstant from '../../../../shared/constants/server';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit, AfterViewInit {
  @ViewChild('tableWrapper') tableWrapper: ElementRef;
  @ViewChild('table') table: ElementRef;
  
  tableWrapperHeight = '100%';
  tabSelected;
  depositShifts;
  noDepositShifts;
  searchText;
  total = 0;

  Math = Math;
  tab = tab;
  serverConstant = serverConstant;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.onClickNoDepositShifts();
  }

  ngAfterViewInit() {
    this.tableWrapperHeight = String(this.tableWrapper.nativeElement.offsetHeight + 'px');
  }

  onClickNoDepositShifts = () => {
    this.tabSelected = tab.NoDepositShifts;
    
    this.http
      .post<ServerResponse>(
        urls.GET_OWE_DRIVERS_REPORT,
        {
            data: {
              idNumber: serverConstant.general.NULL,
              startShiftDt: serverConstant.general.NULL,
              endShiftDt: serverConstant.general.NULL,
              startDepositDt: serverConstant.general.NULL,
              cashBoxId: serverConstant.general.NULL,
              branchCode: serverConstant.general.NULL,
              endDepositDt: serverConstant.general.NULL,
              deposited: serverConstant.deposited.WITHOUT,
              detailsSumCode: serverConstant.detailsSumCode.SUM,
              alsoOpenShifts: serverConstant.alsoOpenShifts.TRUE,
            },
            sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
        })
        .subscribe(
          res => {
            this.noDepositShifts = res.data.filter(shift => shift.remainder != 0);;
          }
        );
  }

  onClickDepositShifts = () => {
    this.tabSelected = tab.depositShifts;

    this.http
      .post<ServerResponse>(
        urls.GET_OWE_DRIVERS_REPORT,
        {
            data: {
              idNumber: serverConstant.general.NULL,
              startShiftDt: serverConstant.general.NULL,
              endShiftDt: serverConstant.general.NULL,
              startDepositDt: serverConstant.general.NULL,
              cashBoxId: serverConstant.general.NULL,
              branchCode: serverConstant.general.NULL,
              endDepositDt: serverConstant.general.NULL,
              deposited: serverConstant.deposited.JUST,
              detailsSumCode: serverConstant.detailsSumCode.SUM,
              alsoOpenShifts: serverConstant.alsoOpenShifts.TRUE,
            },
            sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
        })
        .subscribe(
          res => {
            this.depositShifts = res.data.filter(shift => shift.remainder != 0);
          }
        );
    }

    calcTotal(index, amonut) {
      if (index == 0) {
        this.total = 0;
      }
      this.total += parseFloat(amonut); 
    }

    getFileName() {
      return `דוח נהגים חייבים - משמרות ש${this.tabSelected == tab.NoDepositShifts ? "לא " : "" }נפרקו`;
    }
}

enum tab {
  depositShifts = "depositShifts",
  NoDepositShifts = "NoDepositShifts"
} 
