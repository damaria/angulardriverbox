import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ServerResponse } from 'src/app/shared/constants/server';
import { urls } from 'src/app/shared/constants/urls';
import * as serverConstant from '../../../../shared/constants/server';
import { ActivatedRoute, Params } from '@angular/router';
import { getCleanDate, getCleanTime } from 'src/app/shared/utils/date';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit, AfterViewInit {
  @ViewChild('tableWrapper') tableWrapper: ElementRef;
  
  tableWrapperHeight = '100%';
  shifts;
  driverId;
  driverName;
  deposited;
  searchText;
  tableTitle;
  total = 0;

  Math = Math;
  getCleanDate = getCleanDate;
  getCleanTime = getCleanTime;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.driverId = params['id'];
        this.deposited = params['deposited'];
      }
    );
      
    this.driverName = history.state.name;
    
    this.tableTitle = `פירוט חוב ${this.deposited  == 0 ? "לפני" : "לאחר" } פריקה לנהג ${this.driverName} ${this.driverId}`;

    this.http
      .post<ServerResponse>(
        urls.GET_OWE_DRIVERS_REPORT,
        {
            data: {
              idNumber: this.driverId,
              startShiftDt: serverConstant.general.NULL,
              endShiftDt: serverConstant.general.NULL,
              startDepositDt: serverConstant.general.NULL,
              cashBoxId: serverConstant.general.NULL,
              branchCode: serverConstant.general.NULL,
              endDepositDt: serverConstant.general.NULL,
              deposited: this.deposited,
              detailsSumCode: serverConstant.detailsSumCode.DETAILS,
              alsoOpenShifts: serverConstant.alsoOpenShifts.TRUE,
            },
            sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
        })
        .subscribe(
          res => {
            this.shifts = res.data.filter(shift => shift.remainder != 0);
          }
        );
  }

  calcTotal(index, amonut) {
    if (index == 0) {
      this.total = 0;
    }
    this.total += parseFloat(amonut);
  }

  ngAfterViewInit() {
    this.tableWrapperHeight = String(this.tableWrapper.nativeElement.offsetHeight + 'px');
  }
}
