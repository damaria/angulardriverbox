import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { StoreService } from 'src/app/shared/services/store.service';

@Injectable({ providedIn: 'root' })
export class MainResolverService implements Resolve<any> {
  constructor(
    private storeService: StoreService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.storeService.updateCashBox().subscribe();
  }
}
