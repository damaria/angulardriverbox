import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { getValue, ServerResponse } from '../../../shared/constants/server';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../../login/login.service';
import { urls } from '../../../shared/constants/URLs';
import * as serverConstant from '../../../shared/constants/server';
import { StoreService } from 'src/app/shared/services/store.service';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';

@Component({
  selector: 'app-deposit-to-chashbox',
  templateUrl: './deposit-to-chashbox.component.html',
  styleUrls: ['./deposit-to-chashbox.component.css']
})
export class DepositToChashboxComponent implements OnInit, OnDestroy {
  depositCashbox: CashboxWithoutPapers;
  amountInputChangedSub: Subscription;

  constructor(
    private amountInputService: AmountInputService,
    private http: HttpClient,
    private loginService: LoginService,
    private storeService: StoreService
    ) { }

  ngOnInit(): void {
    this.depositCashbox = new CashboxWithoutPapers();
    
    this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
      (data: subData) => {
        switch(data.operation) {
          case 'increase':
            this.depositCashbox[data.type]++;
            break;
          case 'decrease':
            this.depositCashbox[data.type]--;
            break;
          case 'change':
            this.depositCashbox[data.type] = data.amount;
            break;
        }
      }
    );
  }

  _getImageUrl(type) {
    return getImageUrl(type);
  }

  _getValue(code) {
    return getValue(code);
  }
  
  resetCashbox() {
    this.depositCashbox = new CashboxWithoutPapers();
  }
  
  onSubmit() {
    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          const shift = JSON.parse(sessionStorage.getItem(sessionKeys.shift));
          const depositData = this.depositCashbox.convertToServerFormat();
          this.http
              .post<ServerResponse>(
                  urls.ADD_DRIVER_CASHBOX,
                  {
                    data: {
                      deviceIdCashier: sessionStorage.getItem(sessionKeys.cashboxId),
                      idNumberDriver: serverConstant.general.NULL,
                      shiftIdCashier: shift.Shift_id || shift.shiftId,
                      driverCashboxData: depositData,
                      shifts: [],
                      jsonObjRes: true,
                      comment: "",
                      actionTypeTransaction: serverConstant.actionTypeTransaction.ENTER_MONEY_PROACTIVELY
                    },
                    sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                  })
                  .subscribe(res => {
                    if (res.description == 'ok') {
                      this.storeService.updateCashBox().subscribe();
                    }
                    else {
                      console.log(res);
                    }
                  })
          this.resetCashbox();
        }
      }
    );
  }

  ngOnDestroy() {
    this.amountInputChangedSub.unsubscribe();
  }
}
