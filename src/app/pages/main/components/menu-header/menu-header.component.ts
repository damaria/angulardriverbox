import { 
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LoginService } from 'src/app/pages/login/login.service';
import { DescriptionNoOk, LoginResponse, LoginResponseType } from 'src/app/pages/login/loginResponse';
import { cashboxDataType } from 'src/app/shared/constants/server';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { StoreService } from 'src/app/shared/services/store.service';

@Component({
  selector: 'app-menu-header',
  templateUrl: './menu-header.component.html',
  styleUrls: ['./menu-header.component.css']
})
export class MenuHeaderComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('topRow') topRow: ElementRef;

  userName;
  cashboxName;
  cashboxSum;
  idfVouchers;
  cancels;
  cashboxUpdateSubscription: Subscription;
  
  //small screens
  linksRowHeight = '100vh';
  
  constructor(
    private storeService: StoreService,
    private router: Router,
    private loginService: LoginService) { }
  
  
  ngOnInit(): void {
    if (sessionStorage.getItem(sessionKeys.username)) {
      this.userName = sessionStorage.getItem(sessionKeys.username);
      this.cashboxName = sessionStorage.getItem(sessionKeys.cashboxName);
      this.cashboxSum = this.storeService.getCashBoxSum();
      this.idfVouchers = this.storeService.getCashbox()[cashboxDataType.idfVoucher];
      this.cancels = this.storeService.getCashbox()[cashboxDataType.cancel];
    }
    
    else {
      this.userName = "אנונימי";
      this.cashboxName = "ללא שם";
      this.cashboxSum = 0;
      this.idfVouchers = 0;
      this.cancels = 0;
    }
    
    this.cashboxUpdateSubscription = this.storeService.cashBoxChanged.subscribe(
      (res: any) => {
        this.cashboxSum = res.cashBoxSum;
        this.idfVouchers = res.cashBox[cashboxDataType.idfVoucher];
        this.cancels = res.cashBox[cashboxDataType.cancel];
      }
    );
  }

  ngAfterViewInit() {
    //big screens (100vh - top)
    if (document.documentElement.clientWidth >= 1800) {
      // this.linksRowHeight = String(document.documentElement.clientHeight - this.topRow.nativeElement.offsetHeight + 'px'); 
      this.linksRowHeight = '714px'; 
    }
  }

  disconnect() {
    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          this.loginService.disconnect()
          .subscribe(
          (res: LoginResponse) => {
              switch(res.type) {
                  case LoginResponseType.descriptionNoOk:
                      switch(res.data) {
                        case DescriptionNoOk.endShift:
                          console.log('בעיה בסגירת משמרת');
                          break;
                        case DescriptionNoOk.logoutFromSession:
                          console.log('בעיה בסגירת סשן');
                          break;
                      }
                      break;
                  case LoginResponseType.logoutFromSessionSuccessfully:
                      console.log('משמרת וסשן נסגרו בהצלחה');
                      const queryParams = {};
                      const operatorId = JSON.parse(sessionStorage.getItem(sessionKeys.sessionData)).operator_id;
                      queryParams[operatorId] = '';
                      this.router.navigate(['/login'], {queryParams: queryParams});
                      sessionStorage.clear();
                      break;
              }
          });
        }
      }
    );
  }

  ngOnDestroy() {
    this.cashboxUpdateSubscription.unsubscribe();
  }
}
