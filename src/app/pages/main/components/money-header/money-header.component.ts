import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { StoreService } from 'src/app/shared/services/store.service';

@Component({
  selector: 'app-money-header',
  templateUrl: './money-header.component.html',
  styleUrls: ['./money-header.component.css']
})
export class MoneyHeaderComponent implements OnInit, OnDestroy{
  cashbox;
  cashboxUpdateSubscription: Subscription;

  constructor(
    public storeService: StoreService
    ) { }

  ngOnInit(): void {
    this.cashbox = this.storeService.getCashbox();
    this.cashboxUpdateSubscription = this.storeService.cashBoxChanged.subscribe(
      (res: any) => {
        this.cashbox = { ...res.cashBox };
      }
    )
  }

  ngOnDestroy() {
    this.cashboxUpdateSubscription.unsubscribe();
  }
}
