import { Component, Input, OnInit } from '@angular/core';
import { getValue } from 'src/app/shared/constants/server';
import { cashboxDataType } from '../../../../../shared/constants/server'

@Component({
  selector: 'app-money-header-component',
  templateUrl: './money-header-component.component.html',
  styleUrls: ['./money-header-component.component.css']
})
export class MoneyHeaderComponentComponent implements OnInit {
  @Input() type;
  @Input() amount;
  
  imageUrl;
  sum;

  constructor() { }

  ngOnInit(): void {     
    if (this.type == cashboxDataType.cancel || this.type == cashboxDataType.idfVoucher) {
      this.imageUrl = "assets/cashboxDataType/" + this.type + ".svg";
    }
    else {
      this.imageUrl = "assets/cashboxDataType/" + this.type + ".png";
    }

    switch(this.type) {
      case cashboxDataType.cancel.toString():
        this.sum = 'ביטולי נסיעה';
        break;
      case cashboxDataType.idfVoucher.toString():
        this.sum = 'שוברי צה"ל';
        break;
      default:
        this.sum = this.amount * getValue(this.type)/100;
        if (this.sum >= 1000) {
          this.sum = this.sum.toLocaleString('en-US');
        }
        this.sum = this.sum + ' ₪';
        break;
    }
    if (this.amount >= 1000) {
      this.amount = this.amount.toLocaleString('en-US');
    }
  }
}
