import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'src/app/pages/login/login.service';
import { ServerResponse } from 'src/app/shared/constants/server';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { urls } from 'src/app/shared/constants/urls';
import * as serverConstant from '../../../../shared/constants/server';
import { UnloadDriverCardService } from '../unload-driver-card.service';

@Component({
  selector: 'app-unload-driver-card',
  templateUrl: './unload-driver-card-enter.component.html',
  styleUrls: ['./unload-driver-card-enter.component.css']
})
export class UnloadDriverCardEnterComponent implements OnInit {
  driverId;
  driverNoFound = false;
  intervalTime = 2500;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient,
    private loginService: LoginService,
    private unloadDriverCardService: UnloadDriverCardService
  ) { }

  ngOnInit(): void {
  }

  Search() {
    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          this.http
          .post<ServerResponse>(
            urls.GET_EMPLOYEE_DETAILS,
            {
              data: {
                  idNumber: this.driverId.toString(),
                  typeEmployee: serverConstant.general.NULL,
                  isActive: serverConstant.general.NULL
              },
              sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            })
            .subscribe(res => {
              switch(res.description) {
                case 'ok':
                  const employee = res.data[0]; //else fire popup havent this driverId
                  const fullName = employee.FirstName + " " + employee.LastName;
                  sessionStorage.setItem(sessionKeys.unloadCardDriverName, fullName);
                  this.router.navigate([this.driverId], { relativeTo: this.route});                  
                  break;
                case 'employee not found':
                  this.openDriverNoFoundPopUp();
                  break;
                default:
                  console.log('GET_EMPLOYEE_DETAILS res no OK');
              }
            });
        }
      }
    );
  }

  openDriverNoFoundPopUp() {
    this.driverNoFound = true;
    setTimeout(() => {
        this.driverNoFound = false;
      },
      this.intervalTime
    );
  }

  closeUnloadDriverCard = () => {
    this.router.navigate(['./main/home']);
  }
}
