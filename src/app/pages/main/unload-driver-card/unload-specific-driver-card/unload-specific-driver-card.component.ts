import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { LoginService } from 'src/app/pages/login/login.service';
import { ServerResponse } from 'src/app/shared/constants/server';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { urls } from 'src/app/shared/constants/urls';
import { getCleanDate, getCleanTime } from 'src/app/shared/utils/date';
import { UnloadDriverCardService } from '../unload-driver-card.service';
import * as serverConstant from '../../../../shared/constants/server';

@Component({
  selector: 'app-unload-specific-driver-card',
  templateUrl: './unload-specific-driver-card.component.html',
  styleUrls: ['./unload-specific-driver-card.component.css']
})
export class UnloadSpecificDriverCardComponent implements OnInit ,OnDestroy {
  @ViewChildren('checkbox') checkboxes: QueryList<ElementRef>;
  @ViewChild('checkboxAll') checkboxAll: ElementRef;
  
  driverId;
  driverName;

  shifts;

  billingAmount = 0;
  shiftsAmount = 0;
  
  selectedShiftsAmount = 0;
  sumToDeposit = 0;
  fileName;

  Math = Math;
  getCleanDate = getCleanDate;
  getCleanTime = getCleanTime;

  popUpIsActive = false;
  intervalTime = 2500;

  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private loginService: LoginService,
    public unloadDriverCardService: UnloadDriverCardService
    ) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.driverId = params['id'];
      }
    );
    this.driverName = sessionStorage.getItem(sessionKeys.unloadCardDriverName);

    this.fileName = `משמרות לפריקה לנהג ${this.driverName} ${this.driverId}`;
    
    // ======= check debts from the past ==================

    // this.loginService.checkLogin().subscribe(
    //   connectStatus => {
    //     if (connectStatus == true) {
    //       this.http
    //       .post<ServerResponse>(
    //         urls.GET_DEBT_REPAYMENT_DETAILS,
    //         {
    //           data: {
    //             idNumber: this.driverId.toString(),
    //             startDt: serverConstant.general.NULL,
    //             endDt: serverConstant.general.NULL,
    //           },
    //           sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
    //         })
    //         .subscribe(res => {
    //           if (res.description !== 'ok') {
    //               console.log(res);
    //           }
    //           else {
    //             console.log(res);
    //           }
    //         });
    //     }
    //   }
    // );

    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          this.http
          .post<ServerResponse>(
            urls.GET_SHIFT_TO_DEPOSIT,
            {
              data: {
                  idNumber: this.driverId.toString(),
                  startDt: "0000000000000",
                  endDt: new Date().getTime().toString(),
                  jsonObjRes: true
              },
              sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
            })
            .subscribe(res => {
              if (res.description == 'ok') {
                const allShifts = res.data;
                this.shifts = allShifts;
                this.shifts.forEach(shift => {
                  this.billingAmount += +shift.CashAmountSum;
                });
                this.shiftsAmount = this.shifts.length;
                this.checkAll(this.shifts, true);
              }
              else {
                console.log('GET_SHIFT_TO_DEPOSIT res no OK');
              }
            });
        }
      }
    );
  }

  checkAll(shifts, value) {
    this.unloadDriverCardService.onSelectAll(shifts, value);

    let isChecked;
    value == true ? isChecked = true : isChecked = value.currentTarget.checked;
    if (this.checkboxes.length != 0) {
      if (isChecked) {
        this.checkboxes.forEach(checkbox => {
          checkbox.nativeElement.checked = true;
        })
      }
      else {
        this.checkboxes.forEach(checkbox => {
          checkbox.nativeElement.checked = false;
        })
      }
    }
  }

  onDeposit() {
    if (this.unloadDriverCardService.selectedShifts.length == 0) {
      this.openPopUp();
    }
    else {
      this.router.navigate(['./deposit'], { relativeTo: this.route });
      sessionStorage.setItem(sessionKeys.unloadCardShiftsToDeposit, JSON.stringify(this.unloadDriverCardService.selectedShifts));
    }
  }

  openPopUp() {
    this.popUpIsActive = true;
    setTimeout(() => {
        this.popUpIsActive = false;
      },
      this.intervalTime
    );
  }

  closeUnloadSpecificDriverCard = () => {
    this.router.navigate(['../'], { relativeTo: this.route});
  }

  ngOnDestroy() {
    this.unloadDriverCardService.clearSelectedShifts();
  }
}
