import { Injectable } from "@angular/core";

@Injectable({providedIn: 'root'})
export class UnloadDriverCardService {
    selectedShifts: {index, shift} [] = [];

    onSelect(selectedShift, selectedIndex, value) {
        const isChecked = value.currentTarget.checked;
        if (isChecked) {
            this.selectedShifts.push({
                index: selectedIndex,
                shift: selectedShift
            })
        }
        else {
            this.selectedShifts = this.selectedShifts.filter(item => item.index != selectedIndex);
        }
    }

    onSelectAll(shifts, value) {
        this.clearSelectedShifts();
        
        let isChecked;
        value == true ? isChecked = true : isChecked = value.currentTarget.checked;
        if (isChecked) {
            let selectedIndex = 0;
            shifts.forEach(selectedShift => {
                this.selectedShifts.push({
                    index: selectedIndex,
                    shift: selectedShift
                })
                selectedIndex++;
            });
        }
    }

    getSumSelectedShifts() {
        let sum = 0;
        this.selectedShifts.forEach(item => {
            sum += +item.shift.CashAmountSum;
        });
        return sum;
    }

    isExist(i) {
        let isExist = false;
        if (this.selectedShifts.length != 0) {
            this.selectedShifts.forEach(item => {
                if (item.index == i) {
                    isExist = true;
                }
            })
        }
        return isExist;
    }

    clearSelectedShifts() {
        this.selectedShifts = [];
    }
}