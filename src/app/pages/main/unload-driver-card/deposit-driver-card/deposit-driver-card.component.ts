import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { LoginService } from 'src/app/pages/login/login.service';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
import { urls } from 'src/app/shared/constants/URLs';
import { Cashbox } from 'src/app/shared/models/cashbox.model';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import * as serverConstant from '../../../../shared/constants/server';
import { cashboxDataType as type, getValue, ServerResponse} from 'src/app/shared/constants/server';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { StoreService } from 'src/app/shared/services/store.service';


@Component({
  selector: 'app-deposit-driver-card',
  templateUrl: './deposit-driver-card.component.html',
  styleUrls: ['./deposit-driver-card.component.css']
})
export class DepositDriverCardComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChildren('idfVoucherCheckbox') idfVoucherCheckboxes: QueryList<ElementRef>;
  @ViewChild('idfVoucherCheckboxAll') idfVoucherCheckboxAll: ElementRef;
  
  @ViewChildren('cancelCheckbox') cancelsCheckboxes: QueryList<ElementRef>;
  @ViewChild('cancelCheckboxAll') cancelCheckboxAll: ElementRef;
  
  @ViewChild('cancelsDiv') cancelsDiv: ElementRef;
  cancelsIdfVouchersHeight = '100%';

  driverId;
  driverName;

  shifts;
  billingSum = 0;
  cancels = [];
  selectedCancels = [];
  idfVouchers = [];
  selectedIdfVouchers = [];
  noSelectedPapers;
  noSelectedPapersSum;

  depositCashbox: Cashbox;
  paybackCashbox: CashboxWithoutPapers;
  amountInputChangedSub: Subscription;

  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;
  
  currCashbox: Cashbox;

  moreThanMax = false;
  lessThanZero = false;
  intervalTime = 2500;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private amountInputService: AmountInputService,
    private http: HttpClient,
    private loginService: LoginService,
    private modalService: ModalService,
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.driverId = params['id'];
      }
    );

    this.driverName = sessionStorage.getItem(sessionKeys.unloadCardDriverName);
    this.shifts = JSON.parse(sessionStorage.getItem(sessionKeys.unloadCardShiftsToDeposit));

    this.depositCashbox = new Cashbox();
    this.paybackCashbox = new CashboxWithoutPapers();
    this.currCashbox = new Cashbox();

    // this.checkAll('idfVouchers', true);
    // this.checkAll('cancels', true);

    this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
      (data: subData) => {
        switch(data.operation) {
          case 'increase':
            switch(data.info) {
              case 'deposit':
                this.depositCashbox[data.type]++;
                break;
              case 'payback':
                this.paybackCashbox[data.type]++;
            }
            break;
          case 'decrease':
            switch(data.info) {
              case 'deposit':
                this.depositCashbox[data.type]--;
                break;
              case 'payback':
                this.paybackCashbox[data.type]--;
            }
            break;
          case 'change':
            switch(data.info) {
              case 'deposit':
                this.depositCashbox[data.type] = data.amount;
                break;
              case 'payback':
                this.paybackCashbox[data.type] = data.amount;
            }
            break;
          case 'moreThanMax':
            this.openPopUp('moreThanMax');
            break;
          case 'lessThanZero':
            this.openPopUp('lessThanZero');
            break;
        }
      }
    );
  }
  
  ngAfterViewInit() {
    this.cancelsIdfVouchersHeight = String(this.cancelsDiv.nativeElement.offsetHeight + 'px');
    this.shifts.forEach(item => {
      this.billingSum += +item.shift.CashAmountSum;

      if (item.shift.cancellationDetails.length > 0) {
        this.cancels.push(...item.shift.cancellationDetails);
      } 
      if (item.shift.idfPaperDetails.length > 0) {
        this.idfVouchers.push(...item.shift.idfPaperDetails);
      }
    });
  }

  _getImageUrl(type) {
    return getImageUrl(type);
  }

  _getValue(code) {
    return getValue(code);
  }

  openPopUp(error) {
    switch(error) {
      case 'moreThanMax':
        this.moreThanMax = true;

        setTimeout(() => {
        this.moreThanMax = false;
        }, this.intervalTime);
        break;
      case 'lessThanZero':
        this.lessThanZero = true;

        setTimeout(() => {
        this.lessThanZero = false;
        }, this.intervalTime);
        break;
    }
  }

  closeDepositDriverCard = () => {
    this.router.navigate(['../'], { relativeTo: this.route});
  }

  onCheck(paperType, selectedPaper, value) {
    const isChecked = value.currentTarget.checked;
    if (isChecked) {
      switch(paperType) {
        case 'idfVouchers':
          this.selectedIdfVouchers.push(selectedPaper);
          this.depositCashbox[type.idfVoucher]++;
          break;
        case 'cancels':
          this.selectedCancels.push(selectedPaper);
          this.depositCashbox[type.cancel]++;
          break;
      }
    }
    else {
      switch(paperType) {
        case 'idfVouchers':
          this.selectedIdfVouchers = this.selectedIdfVouchers.filter(item => item.Validation_num != selectedPaper.Validation_num);
          this.depositCashbox[type.idfVoucher]--;
          break;
        case 'cancels':
          this.selectedCancels = this.selectedCancels.filter(item => item.Validation_num != selectedPaper.Validation_num);
          this.depositCashbox[type.cancel]--;
          break;
      }
    }
  }

  checkAll(paperType, value) {
    let isChecked;
    value == true ? isChecked = true : isChecked = value.currentTarget.checked;

    if (this.idfVoucherCheckboxes) {
      if (isChecked) {
        switch(paperType) {
          case 'idfVouchers':
            this.idfVoucherCheckboxes.forEach(checkbox => {
              checkbox.nativeElement.checked = true;
            });
            this.selectedIdfVouchers = this.idfVouchers;
            this.depositCashbox[type.idfVoucher] = this.idfVouchers.length;
            break;
          case 'cancels':
            this.cancelsCheckboxes.forEach(checkbox => {
              checkbox.nativeElement.checked = true;
            })
            this.selectedCancels = this.cancels;
            this.depositCashbox[type.cancel] = this.cancels.length;
            break;
        }
      }
      else {
        switch(paperType) {
          case 'idfVouchers':
            this.idfVoucherCheckboxes.forEach(checkbox => {
              checkbox.nativeElement.checked = false;
            });
            this.selectedIdfVouchers = [];
            break;
          case 'cancels':
            this.cancelsCheckboxes.forEach(checkbox => {
              checkbox.nativeElement.checked = false;
            });
            this.selectedCancels = [];
            break;
        }
      }
    }
  }

  getPayback() {
    return this.depositCashbox.getSum() - this.billingSum - this.paybackCashbox.getSum();
  }

  onClickDeposit() {
    this.getNoSelectedPapers();
    if (this.noSelectedPapers.length > 0) {
      this.modalService.close('deposit-driver-card-modal');
      this.openPapersModal();
    }
    else {
      const payback = this.getPayback();
      switch(true) {
        case (payback == 0):
          this.loginService.checkLogin().subscribe(
            connectStatus => {
              if (connectStatus == true) {
                this.deposit();
              }
            }
          );
          break;
        case (payback < 0):
          this.modalService.close('deposit-driver-card-modal');
          this.openPartialDepositModal();
          break;
        case (payback > 0):
          this.currCashbox = this.storeService.getCashbox();
          this.currCashbox.increase(this.depositCashbox);
          if (!this.hasMoneyForPayback()) {
            this.openNoPaybackModal();
          }
          else {
            this.openPaybackModal();
          } 
          break;
      }
    }
  }

  deposit = () => {
    const shift = JSON.parse(sessionStorage.getItem(sessionKeys.shift));

    // const depositData = this.depositCashbox.convertToServerFormat();
    
    let updatedCashbox = new Cashbox();
    updatedCashbox.increase(this.depositCashbox);
    updatedCashbox.decrease(this.paybackCashbox);
    const cashboxData = updatedCashbox.convertToServerFormat();

    let depositShifts = JSON.parse(sessionStorage.getItem(sessionKeys.unloadCardShiftsToDeposit));
    depositShifts = this.convertShiftsToServerFormat(depositShifts);
    const selectedPapers = this.getConvertedSelectedPapers();

    this.http.
      post<ServerResponse>(
        urls.ADD_DRIVER_CASHBOX,
        {
          data: {
            deviceIdCashier: sessionStorage.getItem(sessionKeys.cashboxId),
            idNumberDriver: this.driverId,
            shiftIdCashier: shift.Shift_id || shift.shiftId,
            driverCashboxData: cashboxData, 
            shifts: depositShifts,
            jsonObjRes: true,
            comment: "",
            markTransactions: selectedPapers,
            actionTypeTransaction: serverConstant.actionTypeTransaction.ADD_TRANSACTION
          },
          sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
        }
      )
      .subscribe(res => {
        if (res.description == "ok") {
          this.modalService.close('deposit-driver-card-modal');
          this.storeService.updateCashBox().subscribe();
          this.openSuccessModal();
        }
        else {

        }
      });
  }

  getNoSelectedPapers() {
    this.noSelectedPapers = [];
    this.noSelectedPapersSum = 0;

    this.idfVouchers.forEach(paper => {
      let found = false;
      for (let i = 0; i < this.selectedIdfVouchers.length; i++) {
        if (this.selectedIdfVouchers[i].Validation_num == paper.Validation_num) {
          found = true;
        }
      }
      if (!found) {
        this.noSelectedPapers.push(paper);
        this.noSelectedPapersSum += +paper.priceForCashbox;
      }
    });

    this.cancels.forEach(paper => {
      let found = false;
      for (let i = 0; i < this.selectedCancels.length; i++) {
        if (this.selectedCancels[i].Validation_num == paper.Validation_num) {
          found = true;
        }
      }
      if (!found) {
        this.noSelectedPapers.push(paper);
        this.noSelectedPapersSum += +paper.priceForCashbox;
      }
    });
  }

  convertShiftsToServerFormat(shifts) {
    let convertedShifts = [];
    shifts.forEach(item => {
      const shift = item.shift;
      convertedShifts.push(
        {
          "deviceId": shift.Device_id,
          "idNumber": shift.idNumber,
          "operator_id": shift.operator_id,
          "shiftId": shift.Shift_ID,
          "shiftStart" : shift.Shift_Start
        }
      )
    });
    return convertedShifts;
  }

  getConvertedSelectedPapers() {
    let convertedSelectedPapers = [];
    let selectedPapers = this.selectedCancels.concat(this.selectedIdfVouchers);
    selectedPapers.forEach(paper => {
      convertedSelectedPapers.push(
        {
          "operator_id": sessionStorage.getItem(sessionKeys.operatorId),
          "Validation_num": paper.Validation_num  
        }
      );
    });

    return convertedSelectedPapers;
  }

  openSuccessModal() {
    this.infoModalIcon = "assets/icons/infoIcons/ok.svg";
    this.infoModalTitle = "כרטיס נפרק בהצלחה";
    this.infoModalSubTitle = null;
    this.infoModalHr = true;
    this.infoModalBtnTitle = "להדפיס אסמכתא?"
    this.infoModalBtnLeftText = "הדפס אסמכתא";
    this.infoModalBtnLeftClickFun = this.closeUnloadDriverCard; //will change in the future..
    this.infoModalBtnRightText = "ללא אסמכתא";
    this.infoModalBtnRightClickFun = this.closeUnloadDriverCard;
    this.infoModalBackgroundClickFunction = this.closeUnloadDriverCard;

    this.modalService.open('info-modal');
  }

  openPartialDepositModal() {
    this.infoModalIcon = "assets/icons/infoIcons/error.svg";
    this.infoModalTitle = "לא הובא כל הסכום הדרוש";
    this.infoModalSubTitle = null;
    this.infoModalHr = true;
    this.infoModalBtnTitle = "האם לבצע הפקדה חלקית?"
    this.infoModalBtnLeftText = "כן";
    this.infoModalBtnLeftClickFun = this.deposit;
    this.infoModalBtnRightText = "לא, חזור להשלמת הסכום";
    this.infoModalBtnRightClickFun = this.closeInfoModalAndReturn;
    this.infoModalBackgroundClickFunction = null;

    this.modalService.open('info-modal');
  }

  openPapersModal() {
    this.infoModalIcon = "assets/icons/infoIcons/error.svg";
    this.infoModalTitle = "לא הובאו כל הכרטיסים";
    this.infoModalSubTitle = [`נדרש להשלים ${this.noSelectedPapersSum.toFixed(2)} ש"ח במזומן`]
    this.infoModalHr = true;
    this.infoModalBtnTitle = "האם להמיר למזומן?"
    this.infoModalBtnLeftText = "כן";
    this.infoModalBtnLeftClickFun = this.convertNoSelectedPapersToCash;
    this.infoModalBtnRightText = "לא, חזור להפקדת הכרטיסים";
    this.infoModalBtnRightClickFun = this.closeInfoModalAndReturn;
    this.infoModalBackgroundClickFunction = null;

    this.modalService.open('info-modal');
  }

  openPaybackModal() {
    this.modalService.close('deposit-driver-card-modal');
    this.modalService.open('payback-modal');
  }

  openNoPaybackModal() {
    this.infoModalIcon = "assets/icons/infoIcons/error.svg";
    this.infoModalTitle = "אין עודף בקופה";
    this.infoModalSubTitle = null;
    this.infoModalHr = null;
    this.infoModalBtnTitle = null;
    this.infoModalBtnLeftText = "אישור";
    this.infoModalBtnLeftClickFun = this.closeInfoModalAndReturn;
    this.infoModalBtnRightText = null;
    this.infoModalBtnRightClickFun = null;
    this.infoModalBackgroundClickFunction = this.closeInfoModalAndReturn;

    this.modalService.close('deposit-driver-card-modal');
    this.modalService.open('info-modal');
  }

  closePaybackModal() {
    this.modalService.close('payback-modal');
    this.modalService.open('deposit-driver-card-modal');
  }

  finshPayback() {
    this.modalService.close('payback-modal');
    this.deposit();
  }

  closeUnloadDriverCard = () => {
    this.router.navigate(['./main/home']);
  }

  closeInfoModalAndReturn = () => {
    this.modalService.close('info-modal');
    this.modalService.open('deposit-driver-card-modal');
  }

  convertNoSelectedPapersToCash = () => {
    this.idfVouchers = this.selectedIdfVouchers;
    this.cancels = this.selectedCancels;
    this.billingSum += this.noSelectedPapersSum;
    this.closeInfoModalAndReturn();
  }

  hasMoneyForPayback = () => {
    let currCashboxCopy = this.storeService.getCashbox();
    let remaindPayback = this.getPayback();

    let coinType = 10; //200 nis
    while (coinType > 0) {
      const coinValue = getValue(coinType)/100;
      while (remaindPayback >= coinValue && currCashboxCopy[coinType] > 0) {
        remaindPayback -= coinValue;
        currCashboxCopy[coinType]--;
      }
      if (remaindPayback == 0) {
        return true;
      }
      coinType--;
    }
    return false;
  }

  ngOnDestroy() {
    this.amountInputChangedSub.unsubscribe();
  }
}
