import { Component, OnDestroy } from '@angular/core';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';

@Component({
  selector: 'app-unload-driver-card-router',
  template: '<router-outlet></router-outlet>'
})
export class UnloadDriverCardRouterComponent implements OnDestroy { 
  ngOnDestroy() {
    sessionStorage.removeItem(sessionKeys.unloadCardDriverName);
    sessionStorage.removeItem(sessionKeys.unloadCardShiftsToDeposit);
  }
}