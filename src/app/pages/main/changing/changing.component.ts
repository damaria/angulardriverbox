import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { getValue, ServerResponse } from 'src/app/shared/constants/server';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { LoginService } from '../../login/login.service';
import { urls } from '../../../shared/constants/URLs';
import * as serverConstant from '../../../shared/constants/server';
import { StoreService } from 'src/app/shared/services/store.service';
import { Cashbox } from 'src/app/shared/models/cashbox.model';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';
// import { popUpService } from 'src/app/shared/components/pop-up/pop-up.service';

@Component({
  selector: 'app-changing',
  templateUrl: './changing.component.html',
  styleUrls: ['./changing.component.css']
})
export class ChangingComponent implements OnInit, OnDestroy {
  currCashbox: Cashbox;
  cashboxIn: CashboxWithoutPapers;
  cashboxOut: CashboxWithoutPapers;
  amountInputChangedSub: Subscription;
  moreThanMax = false;
  lessThanZero = false;
  changingNoEqual = false;
  intervalTime = 3000;
  
  constructor(
    private amountInputService: AmountInputService,
    private loginService: LoginService,
    private http: HttpClient,
    private storeService: StoreService,
    // private popUpService: popUpService
    ) { }

  ngOnInit(): void {
    this.currCashbox = this.storeService.getCashbox();
    this.cashboxIn = new CashboxWithoutPapers();
    this.cashboxOut = new CashboxWithoutPapers();

    this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
      (data: subData) => {
        switch(data.operation) {
          case 'increase':
            data.payload == 'in' ?
            this.cashboxIn[data.type]++ :
            this.cashboxOut[data.type]++;
            break;
          case 'decrease':
            data.payload == 'in' ?
            this.cashboxIn[data.type]-- :
            this.cashboxOut[data.type]--;
            break;
          case 'change':
            data.payload == 'in' ?
            this.cashboxIn[data.type] = data.amount :
            this.cashboxOut[data.type] = data.amount;
            break;
          case 'moreThanMax':
            this.openPopUp('moreThanMax');
            break;
          case 'lessThanZero':
            this.openPopUp('lessThanZero');
            break;
          case 'changingNoEqual':
            this.openPopUp('changingNoEqual');
            break;
        }
      }
    );
  }
  
  _getImageUrl(type) {
    return getImageUrl(type);
  }

  _getValue(code) {
    return getValue(code);
  }  
  
  resetCashboxes() {
    this.cashboxIn = new CashboxWithoutPapers();
    this.cashboxOut = new CashboxWithoutPapers();
  }

  openPopUp(error) {
    switch(error) {
      case 'moreThanMax':
        this.moreThanMax = true;

        setTimeout(() => {
        this.moreThanMax = false;
        }, this.intervalTime);
        break;
      case 'lessThanZero':
        this.lessThanZero = true;

        setTimeout(() => {
        this.lessThanZero = false;
        }, this.intervalTime);
        break;
      case 'changingNoEqual':
        this.changingNoEqual = true;

        setTimeout(() => {
        this.changingNoEqual = false;
        }, this.intervalTime);
        break;
    }
  }

  onSubmit() {
    if (this.cashboxIn.getSum() != this.cashboxOut.getSum()) {
      this.openPopUp('changingNoEqual');
    }
    else {
      this.loginService.checkLogin().subscribe(
        connectStatus => {
          if (connectStatus == true) {
            let updatedCashbox = new CashboxWithoutPapers();
            updatedCashbox.increase(this.cashboxIn);
            updatedCashbox.decrease(this.cashboxOut);
            const cashboxData = updatedCashbox.convertToServerFormat();
            const shift = JSON.parse(sessionStorage.getItem(sessionKeys.shift));

            this.http
                .post<ServerResponse>(
                    urls.ADD_DRIVER_CASHBOX,
                    {
                      data: {
                        deviceIdCashier: sessionStorage.getItem(sessionKeys.cashboxId),
                        idNumberDriver: serverConstant.general.NULL,
                        shiftIdCashier: shift.Shift_id || shift.shiftId,
                        driverCashboxData: cashboxData,
                        shifts: [],
                        jsonObjRes: true,
                        comment: "",
                        actionTypeTransaction: serverConstant.actionTypeTransaction.FIX_CASHBOX_AMOUNT_PROACTIVELY
                      },
                      sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
                    })
                    .subscribe(res => {
                      if (res.description == 'ok') {
                        this.storeService.updateCashBox().subscribe();
                      }
                      else {
                        console.log(res);
                      }
                    })
            this.resetCashboxes();
          }
        }
      );
    }
  }

  ngOnDestroy() {
    this.amountInputChangedSub.unsubscribe();
  }
}
