import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  //small screens
  routerOutletRowHeight = '100vh';

  constructor(private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    //big screens (100vh - moneyHeader)
    if (document.documentElement.clientWidth >= 1800) {
      this.routerOutletRowHeight = String(document.documentElement.clientHeight - 223.1 + 'px');
    }
    if (this.router.url == "/main") {
      this.router.navigate(['./home'], { relativeTo: this.route });
    }
  }
}
