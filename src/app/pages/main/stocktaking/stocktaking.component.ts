import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AmountInputService, subData } from 'src/app/shared/components/amount-input/amount-input.service';
import { getValue, ServerResponse } from 'src/app/shared/constants/server';
import { CashboxWithoutPapers } from 'src/app/shared/models/cashboxWithoutPapers.model';
import { StoreService } from 'src/app/shared/services/store.service';
import { getImageUrl } from 'src/app/shared/utils/cashbox';
import { LoginService } from '../../login/login.service';
import { urls } from '../../../shared/constants/URLs';
import * as serverConstant from '../../../shared/constants/server';
import { Router } from '@angular/router';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { CompareResult } from 'src/app/shared/constants/driverBox';
import { DescriptionNoOk, LoginResponse, LoginResponseType } from '../../login/loginResponse';
import { StocktakingService } from './stocktaking.service';
import { stocktakingResponse } from './stocktakingResponse';
import { sessionKeys } from 'src/app/shared/constants/sessionKey';

@Component({
  selector: 'app-stocktaking',
  templateUrl: './stocktaking.component.html',
  styleUrls: ['./stocktaking.component.css']
})
export class StocktakingComponent implements OnInit, OnDestroy {
  stocktakingCashbox: CashboxWithoutPapers;
  amountInputChangedSub: Subscription;
  responseFromServerSub: Subscription;
  
  infoModalBackgroundClickFunction;
  infoModalIcon;
  infoModalHr;
  infoModalTitle;
  infoModalSubTitle;
  infoModalBtnTitle;
  infoModalBtnRightText;
  infoModalBtnLeftText;
  infoModalBtnRightClickFun;
  infoModalBtnLeftClickFun;
  infoModalError;
  compareResult;
  lockInterval;
  lockIntervalSec = 10;

  constructor(
    private amountInputService: AmountInputService,
    private http: HttpClient,
    private loginService: LoginService,
    private storeService: StoreService,
    private router: Router,
    private modalService: ModalService,
    private stocktakingService: StocktakingService
    ) { }

  ngOnInit(): void {    
    this.stocktakingCashbox = new CashboxWithoutPapers();

    this.responseFromServerSub = this.stocktakingService.responseFromServer.subscribe(
      (res: stocktakingResponse) => {
        if (res == stocktakingResponse.errorFromServer) {
          this.infoModalTitle = "שגיאה בחיבור לשרת";
          this.infoModalIcon = "assets/icons/infoIcons/error.svg";
          this.infoModalBtnLeftText = null;
          this.infoModalBtnLeftClickFun = null;
          this.infoModalBtnRightText = "סגור";
          this.infoModalBtnRightClickFun = this.closeStocktaking;
          this.infoModalBackgroundClickFunction = this.closeStocktaking;
        }
        else {
          if (res == stocktakingResponse.cashboxLocked) {
            this.infoModalTitle = "הקופה ננעלה";
            this.infoModalSubTitle =
            [
              'הקופה ננעלה בעקבות ספירה שגויה',
              `אתה תנותק בעוד ${this.lockIntervalSec} שניות`
            ];
            this.infoModalBtnTitle = null;
            this.infoModalHr = false;
            this.infoModalIcon = "assets/icons/infoIcons/error.svg";
            this.infoModalBtnLeftText = null;
            this.infoModalBtnLeftClickFun = null;
            this.infoModalBtnRightText = "התנתק כעת";
            this.infoModalBtnRightClickFun = this.disconnect;
            this.infoModalBackgroundClickFunction = this.disconnect;

            this.lockInterval = setInterval(() => {
              this.lockIntervalSec--;
              if (this.lockIntervalSec == 0) {
                this.disconnect();
                clearInterval(this.lockInterval);
              }
              else {
                this.infoModalSubTitle =
                [
                  'הקופה ננעלה בעקבות ספירה שגויה',
                  `אתה תנותק בעוד ${this.lockIntervalSec} שניות`
                ];
              }
            }, 1000);
          }
          else {
            this.infoModalTitle = "ספירת מלאי התבצעה בהצלחה";
            this.infoModalBtnTitle = "להדפיס אסמכתא?"
            this.infoModalHr = true;
            this.infoModalIcon = "assets/icons/infoIcons/ok.svg";
            this.infoModalBtnLeftText = "הדפס אסמכתא";
            this.infoModalBtnLeftClickFun = this.closeStocktaking; //will change in the future..
            this.infoModalBtnRightText = "ללא אסמכתא";
            this.infoModalBtnRightClickFun = this.closeStocktaking;
            this.infoModalBackgroundClickFunction = this.closeStocktaking;

            if (res == stocktakingResponse.stocktackingSuccess) {
              this.infoModalSubTitle = ["סכום הספירה זהה לסכום בקופה"];
            }
            else {
              this.storeService.updateCashBox().subscribe();
              if (res == stocktakingResponse.stocktackingSuccessDifferentCoinComposition) {
                // this.infoModalSubTitle = ["שים לב! הרכב המטבעות בספירה לא זהה להרכב המטבעות בקופה"];
                this.infoModalError = "שים לב, הרכב המטבעות בספירה לא זהה להרכב המטבעות בקופה"
              }
              else { //res == stocktakingResponse.stocktackingSuccessDifferentSum
                // this.infoModalSubTitle = ["שים לב! סכום הספירה לא זהה לסכום בקופה"];
                this.infoModalError = "שים לב, סכום הספירה לא זהה לסכום בקופה"
              }
            }
          }
        }
        this.modalService.open('info-modal');
      }
    );
    
    this.amountInputChangedSub = this.amountInputService.inputChanged.subscribe(
      (data: subData) => {
        switch(data.operation) {
          case 'increase':
            this.stocktakingCashbox[data.type]++;
            break;
          case 'decrease':
            this.stocktakingCashbox[data.type]--;
            break;
          case 'change':
            this.stocktakingCashbox[data.type] = data.amount;
            break;
        }
      }
    );
  }

  _getImageUrl(type) {
    return getImageUrl(type);
  }

  _getValue(code) {
    return getValue(code);
  }
  
  resetCashbox() {
    this.stocktakingCashbox = new CashboxWithoutPapers();
  }
  
  onSubmit() {
    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          this.modalService.close('stocktaking-modal');
          
          const realCashbox = this.storeService.getCashbox();
          this.compareResult = realCashbox.compare(this.stocktakingCashbox);
          
          if (this.compareResult == CompareResult.IDENTICAL) {
            this.sendStocktakingToServer();
          }
          else {
            this.infoModalHr = false;
            this.infoModalIcon = "assets/icons/infoIcons/error.svg";
            this.infoModalBtnLeftText = "חזרה לספירה";
            this.infoModalBtnLeftClickFun = this.returnToStocktaking;
            this.infoModalBackgroundClickFunction = this.returnToStocktaking;
            this.infoModalBtnRightText = "אישור וסיום";
            this.infoModalBtnRightClickFun = this.sendStocktakingToServer;

            if (this.compareResult == CompareResult.DIFFERENT_SUM) {
              this.infoModalTitle = "סכום הספירה לא זהה לסכום בקופה";
            }
            else {
              this.infoModalTitle = "הרכב המטבעות בספירה לא זהה להרכב המטבעות בקופה";
            }
            this.modalService.open('info-modal');
          }
        }
      }
    );
  }

  sendStocktakingToServer = () => {
    let gapCashbox = this.stocktakingCashbox.getCopy();
    gapCashbox.decrease(this.storeService.getCashbox());
    const gapData = gapCashbox.convertToServerFormat();
    const shift = JSON.parse(sessionStorage.getItem(sessionKeys.shift)); 
    
    return this.http
      .post<ServerResponse>(
          urls.STOCKTAKING_WITH_LOCK,
          {
            data: {
              actionTypeTransaction: serverConstant.actionTypeTransaction.FIX_CASHBOX_AMOUNT_PROACTIVELY,
              deviceIdCashier: sessionStorage.getItem(sessionKeys.cashboxId),
              idNumberDriver: sessionStorage.getItem(sessionKeys.userId),
              shiftIdCashier:  shift.Shift_id || shift.shiftId,
              comment: "",
              driverCashboxData: gapData
            },
            sessionData: JSON.parse(sessionStorage.getItem(sessionKeys.sessionData))
          })
            .subscribe(
              res => {
                console.log(res);
                if (res.description == 'ok') {
                  switch(this.compareResult) {
                    case CompareResult.IDENTICAL:
                      this.stocktakingService.responseFromServer
                      .next(stocktakingResponse.stocktackingSuccess);
                    break;
                    case CompareResult.DIFFERENT_COIN_COMPOSITION:
                      this.stocktakingService.responseFromServer
                      .next(stocktakingResponse.stocktackingSuccessDifferentCoinComposition);
                      break;
                    case CompareResult.DIFFERENT_SUM:
                      this.stocktakingService.responseFromServer
                      .next(stocktakingResponse.stocktackingSuccessDifferentSum);
                      break;
                  }
                }
                else {
                  if (res.statusCode == 60) {
                    this.stocktakingService.responseFromServer
                    .next(stocktakingResponse.cashboxLocked);
                  }
                  else {
                    this.stocktakingService.responseFromServer
                    .next(stocktakingResponse.errorFromServer);
                  }
                }
              }
            )  
  }

  closeStocktaking = () => {
    this.router.navigate(['./main/home']);
  }

  returnToStocktaking = () => {
    this.modalService.close('info-modal');
    this.modalService.open('stocktaking-modal');
  }

  disconnect = () => {
    this.loginService.checkLogin().subscribe(
      connectStatus => {
        if (connectStatus == true) {
          this.loginService.disconnect()
          .subscribe(
          (res: LoginResponse) => {
              switch(res.type) {
                  case LoginResponseType.descriptionNoOk:
                      switch(res.data) {
                        case DescriptionNoOk.endShift:
                          console.log('בעיה בסגירת משמרת');
                          break;
                        case DescriptionNoOk.logoutFromSession:
                          console.log('בעיה בסגירת סשן');
                          break;
                      }
                      break;
                  case LoginResponseType.logoutFromSessionSuccessfully:
                      console.log('משמרת וסשן נסגרו בהצלחה');
                      const queryParams = {};
                      const operatorId = JSON.parse(sessionStorage.getItem(sessionKeys.sessionData)).operator_id;
                      queryParams[operatorId] = '';
                      this.router.navigate(['/login'], {queryParams: queryParams});
                      break;
              }
          });
        }
      }
    );
  }

  ngOnDestroy() {
    this.amountInputChangedSub.unsubscribe();
    this.responseFromServerSub.unsubscribe();
  }
}
