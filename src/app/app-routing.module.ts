import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent} from './pages/login/login.component';
import { ChangingComponent } from './pages/main/changing/changing.component';
import { DepositToBankComponent } from './pages/main/deposit-to-bank/deposit-to-bank.component';
import { DepositToChashboxComponent } from './pages/main/deposit-to-chashbox/deposit-to-chashbox.component';
import { HomeComponent } from './pages/main/home/home.component';
import { MainResolverService } from './pages/main/main-resolver.service';
import { MainComponent } from './pages/main/main.component';
import { DetailsComponent } from './pages/main/owe-report/details/details.component';
import { GeneralComponent } from './pages/main/owe-report/general/general.component';
import { OweReportComponent } from './pages/main/owe-report/owe-report.component';
import { StocktakingComponent } from './pages/main/stocktaking/stocktaking.component';
import { DepositDriverCardComponent } from './pages/main/unload-driver-card/deposit-driver-card/deposit-driver-card.component';
import { UnloadDriverCardRouterComponent } from './pages/main/unload-driver-card/unload-driver-card-router/unload-driver-card-router.components';
import { UnloadDriverCardEnterComponent } from './pages/main/unload-driver-card/unload-driver-card-enter/unload-driver-card-enter.component';
import { UnloadSpecificDriverCardComponent } from './pages/main/unload-driver-card/unload-specific-driver-card/unload-specific-driver-card.component';
import { DepositHistoryComponent } from './pages/main/deposit-history/deposit-history.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'main', component: MainComponent, resolve: [MainResolverService], children: [
    { path: 'home', component: HomeComponent },
    { path: 'unloadDriverCard', component: UnloadDriverCardRouterComponent, children: [
      { path: '',  component: UnloadDriverCardEnterComponent, pathMatch: 'full'},
      { path: ':id',  component: UnloadSpecificDriverCardComponent},
      { path: ':id/deposit',  component: DepositDriverCardComponent}
    ]},
    { path: 'oweReport', component: OweReportComponent, children: [
      { path: '', component: GeneralComponent, pathMatch: 'full'},
      { path: ':id/:deposited', component: DetailsComponent }
    ]},
    { path: 'depositHistory', component: DepositHistoryComponent },
    { path: 'depositToCashbox', component: DepositToChashboxComponent },
    { path: 'changing', component: ChangingComponent },
    { path: 'stocktaking', component: StocktakingComponent },
    { path: 'depositToBank', component: DepositToBankComponent },
  ]},
    { path: '404', component: NotFoundComponent },
    { path: '**', redirectTo: '/404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
